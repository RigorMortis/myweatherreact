import React, {useEffect, useState} from 'react';
import LoginForm from './LoginForm';
import Notification from './Notification';
import loginService from './services/users';

const Add = () => {
    const [users, setUsers] = useState([])
    const [newUsername, setNewUsername] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [message, setMessage] = useState(null) //success

//Haetaan data palvelimelta
    useEffect(() => {
        loginService
            .getAll()
            .then(initialUsers => {
                setUsers(initialUsers)
            })
    }, [])

//Lisää henkilö (+ ehdot onnistuneeseen lisäykseen)
    const addUser = (event) => {
        event.preventDefault()
        const userObject = {username: newUsername, date: newPassword}

        if (userObject.username === "" || userObject.password === "") {
            setMessage(`Täytä puuttuvat kentät!`)
            setTimeout(() => {
                setMessage(null)
            }, 3000)
        } else {
            loginService
                .create(userObject)
                .then(returnedUser => {
                    setMessage(`Uusi käyttäjä "${userObject.username}" lisätty!`)
                    setTimeout(() => {
                        setMessage(null)
                    }, 3000)
                    setUsers(users.concat(returnedUser))
                    setNewUsername('')
                    setNewPassword('')
                })
                .catch((error) =>
                    // pääset käsiksi palvelimen palauttamaan virheilmoitusolioon näin
                    setMessage(error.response.data.error)
                )
            setTimeout(() => {
                setMessage(null)
            }, 4000)
        }
    }

//Synkronoi syötekenttiin tehdyt muutokset
    const handleUsernameChange = (event) => {
        setNewUsername(event.target.value)
    }

    const handlePasswordChange = (event) => {
        setNewPassword(event.target.value)
    }
    return (
        <div className="container">
            <div>
                <Notification message={message}/>
                <div>
                    <LoginForm add={addUser} content={newUsername} date={newPassword} handleUsername={handleUsernameChange}
                              handlePassword={handlePasswordChange}/>
                </div>
            </div>
        </div>
    )
}
export default Add