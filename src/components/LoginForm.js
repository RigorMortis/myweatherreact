import React from 'react'

const LoginForm = props => {
    return(
        <form onSubmit={props.add}>
            <label> username: </label>
            <input value={props.username}
                   onChange={props.handleUsername}/>

            <label>password:</label>
            <input value={props.psw}
                   onChange={props.handlePassword}/>
            <button type="submit">Login</button>
        </form>
    )
}

export default LoginForm