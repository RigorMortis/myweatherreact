import logo from './logo.svg';
import './App.css';
import Add from './components/Add';

function App() {

  return (
    <div className="App">
      <header className="App-header">
          <h1>MyWeather!</h1>
        <p>Kirjaudu sisään</p>
          <Add />
      </header>
    </div>
  );
}

export default App;
